import {Component, Inject ,Injectable, CORE_DIRECTIVES, FORM_DIRECTIVES, NgModel} from 'angular2/angular2';
import {SwitchService} from '../components/switchService';
import {SwitchResource} from "../components/switchResource";

@Component({
    selector: 'switch-app',
    template: `
    <div class="container-fluid">
     <h1>Lampor</h1>
     <div class="row" *ng-for="#switch of switches">
       <h4>{{switch.deviceName}}</h4>
       <div class="btn-group">
         <button class="btn btn-lg btn-default" (click)="off(switch.deviceId)">Av</button>
         <button class="btn btn-lg btn-primary" (click)="on(switch.deviceId)">På</button>
       </div>
     </div>
    </div>
  `,
    viewProviders: [SwitchService, SwitchResource],
    directives:[CORE_DIRECTIVES]
})
export class App {
    private switches;
    constructor(public switchService: SwitchService){
        switchService.getSwitches().map(res => res.json()).subscribe(res => this.switches = res);
    }

    on(deviceId:number){
        this.switchService.on(deviceId).subscribe();
    }

    off(deviceId:number){
        this.switchService.off(deviceId).subscribe();
    }
}