import {Injectable} from 'angular2/angular2'
import {Switch, SwitchResource} from "./switchResource";

@Injectable()
export class SwitchService {

  constructor(public switchResource: SwitchResource){

  }
  
  getSwitches() {
    return this.switchResource.getSwitches();
  }

  on(switchId: number){
    return this.switchResource.on(switchId);
  }

  off(switchId: number){
    return this.switchResource.off(switchId);
  }
  
}