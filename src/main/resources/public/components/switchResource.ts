import {Injectable} from 'angular2/angular2'
import {Http, Response, HTTP_PROVIDERS} from 'angular2/http';

export interface Switch {
  id: number;
  name: string;
  lighted: boolean;
}

@Injectable()
export class SwitchResource {

  constructor(public _http: Http){
  }
  
  on(switchId: number){
    return this._http.post('device/'+switchId+'/on','');
  }

  off(switchId: number){
    return this._http.post('device/'+switchId+'/off','');
  }

  getSwitches() {
    return this._http.get('device');
  }
}