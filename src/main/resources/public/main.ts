import 'reflect-metadata'
import 'zone.js'

import {bootstrap} from 'angular2/angular2'
import {HTTP_PROVIDERS} from 'angular2/http';
import {App} from "./app/app";

bootstrap(App, [HTTP_PROVIDERS]);

