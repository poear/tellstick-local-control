package org.tellstick.local.repositiry.model;

import lombok.Builder;
import lombok.Data;

@Data
public class Switch {

    @Builder
    public Switch(Integer deviceId, String deviceName, Boolean deviceLighted) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.deviceLighted = deviceLighted;
    }

    private Integer deviceId;
    private String deviceName;
    private Boolean deviceLighted;
}