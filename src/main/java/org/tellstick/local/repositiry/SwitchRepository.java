package org.tellstick.local.repositiry;

import org.springframework.stereotype.Component;
import org.tellstick.local.repositiry.model.Switch;

import java.util.Arrays;
import java.util.List;

@Component
public class SwitchRepository {

    private Switch[] switchList = new Switch[]{
            Switch.builder().deviceId(1).deviceLighted(false).deviceName("Fönsterlampor").build()
            //        ,Switch.builder().deviceId(2).deviceLighted(false).deviceName("Hörnlampa vardagsrum").build()
    };

    public List<Switch> findAll() {
        return Arrays.asList(switchList);
    }

    public Switch findByDeviceId(Integer deviceId) {
        for (Switch s : switchList) {
            if (s.getDeviceId() == deviceId) {
                return s;
            }
        }
        return null;
    }

    public synchronized void switchOn(int deviceId) {
        for (Switch s : switchList) {
            if (s.getDeviceId() == deviceId) {
                s.setDeviceLighted(true);
            }
        }
    }

    public synchronized void switchOff(int deviceId) {
        for (Switch s : switchList) {
            if (s.getDeviceId() == deviceId) {
                s.setDeviceLighted(false);
            }
        }
    }
}
