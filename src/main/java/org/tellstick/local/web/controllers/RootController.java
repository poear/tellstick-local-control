package org.tellstick.local.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.tellstick.local.control.Switchboard;
import org.tellstick.local.repositiry.SwitchRepository;
import org.tellstick.local.repositiry.model.Switch;

import java.util.List;

@Controller
public class RootController {

    @Autowired
    private SwitchRepository switchRepository;

    @Autowired
    private Switchboard switchboard;

    @RequestMapping(value = "device", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Switch> getDevices() {
        return switchRepository.findAll();
    }

    @RequestMapping(value = "device/{deviceId}/on", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void on(@PathVariable(value = "deviceId") int deviceId) {
        switchboard.turnOn(deviceId);
    }

    @RequestMapping(value = "device/{deviceId}/off", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void off(@PathVariable(value = "deviceId") int deviceId) {
        switchboard.turnOff(deviceId);
    }

    @RequestMapping(value = "device/{deviceId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Boolean isLit(@PathVariable(value = "deviceId") int deviceId) {
        return switchboard.isLit(deviceId);
    }
}
