package org.tellstick.local.control;

/**
 * Service providing basic functions from the telldus-core library.
 */
public interface Switchboard {

    void turnOn(int deviceId);

    void turnOff(int deviceId);

    Boolean isLit(int deviceId);
}
