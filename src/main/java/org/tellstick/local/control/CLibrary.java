package org.tellstick.local.control;

import com.sun.jna.Library;

/**
 * jna wrapper for telldus-core
 */
public interface CLibrary extends Library {
    void tdInit();

    void tdClose();

    int tdTurnOn(int deviceId);

    int tdTurnOff(int deviceId);
}

