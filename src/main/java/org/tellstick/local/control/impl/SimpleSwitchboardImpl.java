package org.tellstick.local.control.impl;

import com.sun.jna.Native;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tellstick.local.control.CLibrary;
import org.tellstick.local.control.Switchboard;
import org.tellstick.local.repositiry.SwitchRepository;

import static org.tellstick.local.control.impl.TellstickCodes.TELLSTICK_SUCCESS;

@Service
public class SimpleSwitchboardImpl implements Switchboard {

    private static final Logger logger = LoggerFactory.getLogger(SimpleSwitchboardImpl.class);
    private CLibrary lib;

    @Autowired
    private SwitchRepository switchRepository;

    public SimpleSwitchboardImpl() {
//        this.lib = (CLibrary)Native.loadLibrary("C:\\Program Files (x86)\\Telldus\\Development\\TelldusNETWrapper", CLibrary.class);
        this.lib = (CLibrary) Native.loadLibrary("libtelldus-core.so.2", CLibrary.class);
        this.lib.tdInit();
    }

    public void turnOn(int deviceId) {
        int turnedOn = lib.tdTurnOn(deviceId);
        if (TELLSTICK_SUCCESS == turnedOn) {
            switchRepository.switchOn(deviceId);
        } else {
            logger.warn("tried to turn on device with id {} and got code {} in return.", deviceId, turnedOn);
        }
    }

    public void turnOff(int deviceId) {
        int turnedOff = lib.tdTurnOff(deviceId);
        if (TELLSTICK_SUCCESS == turnedOff) {
            switchRepository.switchOff(deviceId);
        } else {
            logger.warn("tried to turn off device with id {} and got code {} in return.", deviceId, turnedOff);
        }
    }

    @Override
    public Boolean isLit(int deviceId) {
        return switchRepository.findByDeviceId(deviceId).getDeviceLighted();
    }

}
