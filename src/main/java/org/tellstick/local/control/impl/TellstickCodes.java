package org.tellstick.local.control.impl;

/**
 * Success and error codes.
 */
public class TellstickCodes {

    public static final int TELLSTICK_BELL = 4;
    public static final int TELLSTICK_CHANGE_AVAILABLE = 5;
    public static final int TELLSTICK_CHANGE_FIRMWARE = 6;
    public static final int TELLSTICK_CHANGE_METHOD = 4;
    public static final int TELLSTICK_CHANGE_MODEL = 3;
    public static final int TELLSTICK_CHANGE_NAME = 1;
    public static final int TELLSTICK_CHANGE_PROTOCOL = 2;
    public static final int TELLSTICK_CONTROLLER_TELLSTICK = 1;
    public static final int TELLSTICK_CONTROLLER_TELLSTICK_DUO = 2;
    public static final int TELLSTICK_CONTROLLER_TELLSTICK_NET = 3;
    public static final int TELLSTICK_DEVICE_ADDED = 1;
    public static final int TELLSTICK_DEVICE_CHANGED = 2;
    public static final int TELLSTICK_DEVICE_REMOVED = 3;
    public static final int TELLSTICK_DEVICE_STATE_CHANGED = 4;
    public static final int TELLSTICK_DIM = 16;
    public static final int TELLSTICK_DOWN = 256;
    public static final int TELLSTICK_ERROR_BROKEN_PIPE = -9;
    public static final int TELLSTICK_ERROR_COMMUNICATING_SERVICE = -10;
    public static final int TELLSTICK_ERROR_COMMUNICATION = -5;
    public static final int TELLSTICK_ERROR_CONNECTING_SERVICE = -6;
    public static final int TELLSTICK_ERROR_DEVICE_NOT_FOUND = -3;
    public static final int TELLSTICK_ERROR_METHOD_NOT_SUPPORTED = -4;
    public static final int TELLSTICK_ERROR_NOT_FOUND = -1;
    public static final int TELLSTICK_ERROR_PERMISSION_DENIED = -2;
    public static final int TELLSTICK_ERROR_SYNTAX = -8;
    public static final int TELLSTICK_ERROR_UNKNOWN = -99;
    public static final int TELLSTICK_ERROR_UNKNOWN_RESPONSE = -7;
    public static final int TELLSTICK_EXECUTE = 64;
    public static final int TELLSTICK_HUMIDITY = 2;
    public static final int TELLSTICK_LEARN = 32;
    public static final int TELLSTICK_RAINRATE = 4;
    public static final int TELLSTICK_RAINTOTAL = 8;
    public static final int TELLSTICK_STOP = 512;
    public static final int TELLSTICK_SUCCESS = 0;
    public static final int TELLSTICK_TEMPERATURE = 1;
    public static final int TELLSTICK_TOGGLE = 8;
    public static final int TELLSTICK_TURNOFF = 2;
    public static final int TELLSTICK_TURNON = 1;
    public static final int TELLSTICK_TYPE_DEVICE = 1;
    public static final int TELLSTICK_TYPE_GROUP = 2;
    public static final int TELLSTICK_TYPE_SCENE = 3;
    public static final int TELLSTICK_UP = 128;
    public static final int TELLSTICK_WINDAVERAGE = 32;
    public static final int TELLSTICK_WINDDIRECTION = 16;
    public static final int TELLSTICK_WINDGUST = 64;
}
