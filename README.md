# README #

### Controlling Nexa power switches with a Tellstick Net ###

* The objective was to enable switching on/off devices hooked up to my nexa switches for any mobile hooked up to my LAN. I know that this feature is easily obtained with vareous off the shelf products, but i thought it would be a fun project.

### How do I get set up? ###

pi@raspberrypi:~ $ cat /etc/udev/rules.d/99-tellstick.rules
ACTION=="add", ATTRS{idVendor}=="1781", ATTRS{idProduct}=="0c31", SYMLINK+="tellstick"

sudo vim /etc/apt/sources.list.d/telldus.list
`deb-src http://download.telldus.se/debian stable main`
curl http://download.telldus.se/debian/telldus-public.key | sudo apt-key add -
sudo apt-get update && sudo apt-get install build-essential -y
sudo apt-get build-dep telldus-core
mkdir telldus-tmp
cd telldus-tmp/
sudo apt-get install cmake libconfuse-dev libftdi-dev help2man
sudo apt-get --compile source telldus-core
sudo dpkg -install *.deb
sudo dpkg --install *.deb
sudo vim tellstick.conf
`user = "nobody"
 group = "plugdev"
 deviceNode = "/dev/tellstick"
 ignoreControllerConfirmation = "false"
 device {
   id = 1
   name = "Example device"
   controller = 0
   protocol = "arctech"
   model = "codeswitch"
   parameters {
     # devices = ""
     house = "A"
     unit = "1"
     # code = ""
     # system = ""
     # units = ""
     # fade = ""
   }
 }
 controller {
   id = 1
   # name = ""
   type = 2
   serial = "A6028E6A"
 }
`

sudo mkdir -p /var/tellstick-local-control
sudo useradd -s /usr/sbin/nologin -r -M -d /var/tellstick-local-control tellstick
sudo chown tellstick:plugdev /var/tellstick-local-control
sudo cp /home/pi/ROOT-2.1-SNAPSHOT.jar /var/tellstick-local-control
sudo ln -s /var/tellstick-local-control/ROOT-2.1-SNAPSHOT.jar /etc/init.d/tellstick-local-control
sudo update-rc.d tellstick-local-control defaults
sudo service tellstick-local-control start


![Tellstick switchboard.PNG](https://bitbucket.org/repo/Bb9R4X/images/251237658-Tellstick%20switchboard.PNG)